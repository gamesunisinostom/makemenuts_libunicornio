#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
}

void Menu::inicializar()
{
	//	carrega os spritesheets para os botoes
	gRecursos.carregarSpriteSheet("botao_start", "./spritesheets/button_start.png", 3, 1);
    gRecursos.carregarSpriteSheet("botao_credits", "./spritesheets/button_credits.png", 3, 1);
    gRecursos.carregarSpriteSheet("botao_option", "./spritesheets/button_option.png", 3, 1);
    gRecursos.carregarSpriteSheet("botao_exit", "./spritesheets/button_exit.png", 3, 1);

	//	setar spritesheet nos botoes
	jogar.setSpriteSheet("botao_start");
	credits.setSpriteSheet("botao_credits");
	option.setSpriteSheet("botao_option");
	sair.setSpriteSheet("botao_exit");

	//	posiciona os botoes
	jogar.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2 - 100);
	option.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
	credits.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2 + 100);
	sair.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2 + 200);

}

void Menu::finalizar()
{
	//	descarregar spritesheets
    gRecursos.descarregarSpriteSheet("botao_start");
    gRecursos.descarregarSpriteSheet("botao_option");
    gRecursos.descarregarSpriteSheet("botao_credits");
    gRecursos.descarregarSpriteSheet("botao_exit");
}

void Menu::atualiza(Status &status)
{
	//	atualiza todos os botoes
	jogar.atualizar();
	option.atualizar();
	credits.atualizar();
	sair.atualizar();

	if (jogar.estaClicado()){
		status = startGame;
	}
    if (option.estaClicado()) {
        status = inOptions;
    }
    if (credits.estaClicado()) {
        status = telaCreditos;
    }
    if (sair.estaClicado()) {
        status = outGame;
    }
}

void Menu::desenhar()
{
	//	desenha os botoes
	jogar.desenhar();
	option.desenhar();
	credits.desenhar();
	sair.desenhar();
}