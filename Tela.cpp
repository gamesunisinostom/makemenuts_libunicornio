#include "Tela.h"

Tela::Tela() { }

Tela::~Tela() { }

void Tela::inicializar(Texto fonte)
{
    tempoAnima = 0;
    animacao = false;

    nivel.setFonte(fonte.getFonte());
    pontos.setFonte(fonte.getFonte());
    vidas.setFonte(fonte.getFonte());
    creditos.setFonte(fonte.getFonte());

    nivel.setCor(0, 0, 0);
    pontos.setCor(0, 0, 0);
    vidas.setCor(0, 0, 0);
    creditos.setCor(255, 255, 255);

    nivel.setAncora(0, 0);
    pontos.setAncora(0, 0);
    vidas.setAncora(0, 0);
    creditos.setAncora(0.5, 0.5);

    nivel.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    pontos.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    vidas.setAlinhamento(TEXTO_ALINHADO_A_ESQUERDA);
    creditos.setAlinhamento(TEXTO_CENTRALIZADO);

    nivel.setEspacamentoLinhas(1.5f);
    pontos.setEspacamentoLinhas(1.5f);
    vidas.setEspacamentoLinhas(1.5f);
    creditos.setEspacamentoLinhas(1.5f);
    
    nivel.setString("Nivel: ");
    pontos.setString("Pontos: ");
    vidas.setString("Vidas: ");

    gRecursos.carregarSpriteSheet("mostruario", "spritesheets/tela_mostruarios.png", 3, 1);
    telaPonto.setSpriteSheet("mostruario");
    telaVida.setSpriteSheet("mostruario");
    telaNivel.setSpriteSheet("mostruario");

    telaNivel.setAncora(0, 0);
    telaPonto.setAncora(0, 0);
    telaVida.setAncora(0, 0);

}

void Tela::setInterface(int nNivel, int nPonto, int nVida)
{
    setNivel(nNivel);
    setPontos(nPonto);
    setVidas(nVida);
}

void Tela::showCreditos()
{
    creditos.setString(
        "-- Desenvolvedores --"
        "\nThomas Souza && Pietro Paludo\n\n;D"
        "\nDiretor de Arte: Pietro Paludo"
        "\nDiretor de Programa��o: Thomas Souza"
        "\nLevel Design: Thomas Souza"
        "\n Sonoplastia: Pietro Paludo"
        "\nTester: Pietro Paludo"
        "\n\n --- Fontes ---"
        "\nSons: www.freesound.org\nSprites : RPG Maker XP"
        "\n\n -- Agradecimentos Especiais --"
        "\nD�bora Matheus"
        "\n\n\nAperte ENTER para voltar"
    );

    creditos.setCorPalavra("-- Desenvolvedores --", 2, 163, 233);
    creditos.setCorPalavra("--- Fontes ---", 2, 163, 233);
    creditos.setCorPalavra("-- Agradecimentos Especiais --", 2, 163, 233);
    creditos.setCorPalavra("ENTER", 237, 28, 36);

    creditos.desenhar(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
}

void Tela::setNivel(int niv)
{
    string mudada = nivel.getString();
    ostringstream convert;
    convert << niv; 
    mudada = "Nivel: " + convert.str();
    nivel.setString(mudada);
}
void Tela::setPontos(int pont)
{
    string mudada = pontos.getString();
    ostringstream convert;
    convert << pont;
    mudada = "Pontos: " + convert.str();
    pontos.setString(mudada);
}
void Tela::setVidas(int vid)
{
    string mudada = vidas.getString();
    ostringstream convert;
    convert << vid;
    mudada = "Vidas: " + convert.str();
    vidas.setString(mudada);
}

void Tela::desenha()
{
    telaNivel.desenhar((gJanela.getLargura() / 2) - ((gJanela.getLargura() / 2) * 0.5),  15);
    nivel.desenhar((gJanela.getLargura() / 2)  - ((gJanela.getLargura() / 2) * 0.48) , 20);
    telaPonto.desenhar((gJanela.getLargura() / 2) + ((gJanela.getLargura() / 2) * 0.23), 15);
    pontos.desenhar((gJanela.getLargura() / 2) + ((gJanela.getLargura() / 2) * 0.25), 20);
    telaVida.desenhar((gJanela.getLargura() / 2) - ((gJanela.getLargura() / 2) * 0.5), gJanela.getAltura() * 0.92);
    vidas.desenhar((gJanela.getLargura() / 2) - ((gJanela.getLargura() / 2) * 0.48), gJanela.getAltura() * 0.92);
}

void Tela::perdeVida()
{
    telaVida.setAnimacao(2);
}

void Tela::ganhaVida()
{
    telaVida.setAnimacao(1);
}

void Tela::passaNivel()
{
    telaNivel.setAnimacao(1);
}

void Tela::resetMostruarios()
{
    telaNivel.setAnimacao(0);
    telaPonto.setAnimacao(0);
    telaVida.setAnimacao(0);
}

bool Tela::getAnimacao()
{
    return animacao;
}

void Tela::startAnima()
{
    animacao = true;
}

void Tela::addTimer()
{
    if (animacao) {
        if (tempoAnima == 180) {
            tempoAnima = 0;
            animacao = false;
            resetMostruarios();
        } else{
            tempoAnima++;
        }
    }
}