#include "libUnicornio.h"
#include "Tela.h"
#include "Fase.h"
#include "Menu.h"
#include "Personagem.h"
#include <windows.h>
#pragma comment(lib, "user32.lib")

class Jogo
{
public:
    Jogo();
    ~Jogo();

    void inicializar();
    void finalizar();
    void executar();
    void play();
    void nivelar();
    void startPlay();
    void telaInicial();
    void telaOptions();
    void telaCred();
    void telaPausa();
    void telaGameOver();
    void addPontos();
    int getPontos();
    void loadRecursos();
    void setRecursos();
    void atualizaPause(Status &status);
    void desenharPause();
    void testaColisaoVeiculo();
    void testaColisaoObjetiva();

protected:
    Tela tela;
    Fase fase;
    Menu menu;
	Status status;
    Personagem esquilo;
    Texto meuLetreiro;
    BotaoSprite resume, sair, options, somLigar;
    Som pontoYay, morteBoo, trafegoSom, gameOverSom, somGetNoz, somGetLife;
    Sprite fundo, grama, grama1, fundoinicial, gameOver, dropBarril, telaPause, optionsTela;
    string pauseEm;
    bool faseGerada, musica;
    int pontos, contador, nivel;
    int max_resolution_x = GetSystemMetrics(SM_CXSCREEN);
    int max_resolution_y = GetSystemMetrics(SM_CYSCREEN);
};