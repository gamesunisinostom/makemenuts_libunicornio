#include "Carro.h"
#include <iostream>

Carro::Carro() 
{
	colidido = false;
}

Carro::~Carro() { }

void Carro::inicializar(string nomeSprite, float posX, float posY, float speed, char novaDirecao)
{
	spriteCar.setSpriteSheet(nomeSprite);
    spriteCar.setEscala(0.9, 0.9);
	setPosition(posX, posY);
    setVelocidade(speed);
	setDirecao(novaDirecao);
}

void Carro::setSprite(string nomeSprite)
{
	spriteCar.setSpriteSheet(nomeSprite);
}
Sprite Carro::getSprite()
{
	return spriteCar;
}

float Carro::getAltura()
{
	return spriteCar.getAltura();
}
float Carro::getLargura()
{
	return spriteCar.getLargura();
}

void Carro::setDirecao(char novaDirecao)
{
	direcao = novaDirecao;

	if (direcao == 'e')  {
		spriteCar.setAnimacao(0);
	} else {
		spriteCar.setAnimacao(1);
	}
}

void Carro::setPosition(int novoPosX, int novoPosY)
{
    posX = novoPosX;
    posY = novoPosY;
}
int Carro::getPosX()
{
    return posX;
}
int Carro::getPosY()
{
    return posY;
}

void Carro::setVelocidade(float Velocidade)
{
    speed = Velocidade;
}

void Carro::goDireita()
{
    int largura = gJanela.getLargura();

    spriteCar.setAnimacao(1);
    if (posX > gJanela.getLargura() / 2 + gJanela.getLargura() * 0.35) {
        posX = gJanela.getLargura() / 2 - gJanela.getLargura() * 0.3;
        setSprite(randomCar());
    } else {
        posX = posX + speed;
    }
}
void Carro::goEsquerda()
{
    int largura = gJanela.getLargura();

    if (posX < gJanela.getLargura() / 2 - gJanela.getLargura() * 0.35) {
        posX = gJanela.getLargura() / 2 + gJanela.getLargura() * 0.3;
        setSprite(randomCar());
    } else {
        posX = posX - speed;
    }
}

void Carro::changeVelocidade()
{
    srand(time(0));
    speed = rand() % 10 + 5;
}

void Carro::setAnima(int ID)
{
	spriteCar.setAnimacao(ID);
}

void Carro::desenhar(bool status)
{
    if (!colidido && status) {
        if (direcao == 'e') {
            goEsquerda();
        } else {
            goDireita();
        }
        spriteCar.avancarAnimacao();
    }
    
    spriteCar.desenhar(posX, posY);
}

bool Carro::testaColisao(int testX, int testY, float testLargura, float testAltura)
{
    bool colisao;

    int carroX = getPosX();
    int carroY = getPosY();
    float carroAlt = spriteCar.getAltura() - 25;
    float carroLarg = spriteCar.getLargura();

    colisao = uniTestarColisaoRetanguloComRetangulo(
        testX, testY, 0.0, testLargura, testAltura, 0.5, 0,
        carroX, carroY, 0.0, carroLarg, carroAlt, 0.5, 0.5
     );

    return colisao;
}

void Carro::crashCarro()
{
    colidido = true;
}

bool Carro::getColidido()
{
    return colidido;
}

void Carro::setSom(string novoSom)
{
    somCarro.setAudio(novoSom);
}

void Carro::somColisao()
{
    somCarro.tocar();
}

string Carro::randomCar()
{
    int tipo = rand() % 4;
    switch (tipo){
    case 0:
        return "carro";
    case 1:
        return "carro2";
    case 2:
        return "carro3";
    case 3:
        return "carro4";
    }
}