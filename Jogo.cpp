#include "Jogo.h"
#include <ctime>


Jogo::Jogo()
{
}

Jogo::~Jogo()
{
}

void Jogo::inicializar(){
	uniInicializar(max_resolution_x, max_resolution_y, true, "Make me Nuts");

    gJanela.setTamanho(max_resolution_x * 1.2, max_resolution_y * 1.2);

    status = telaIni;
    startPlay();
    musica = true;

    loadRecursos();
    menu.inicializar();
    setRecursos();
	
    esquilo.inicializar("esquilo", 400, 850, 2.3);
    tela.setInterface(1, getPontos(), esquilo.getVidas());

    gMusica.tocar("musMenu", true);
}

void Jogo::finalizar()
{
    gRecursos.descarregarTodasMusicas();
	gRecursos.descarregarTudo();
	menu.finalizar();
	uniFinalizar();
}

void Jogo::executar()
{
	while (!gTeclado.soltou[TECLA_ESC] && !gEventos.sair && status != outGame)
	{
		uniIniciarFrame();

        /* gDebug.depurar("Larg", max_resolution_x * 0.8);
        gDebug.depurar("Alt", max_resolution_y * 0.8);
        gDebug.depurar("janela Alt", gJanela.getAltura());
        gDebug.depurar("janela Larg", gJanela.getLargura());
        gDebug.depurar("fundo Alt", fundoinicial.getAltura()); */

		switch (status) {
            case telaIni: 
                pauseEm = "jogo";
                telaInicial();
				    break;
            case startGame: 
                pauseEm = "jogo";
                status = playGame;
                startPlay();
                break;
			case playGame: play();
                break;
            case telaCreditos: telaCred();
                break;
            case endGame:
                telaGameOver();
                if (gTeclado.pressionou[TECLA_ENTER] || gTeclado.pressionou[TECLA_ENTER2]) {
                    if (musica) {
                        gMusica.tocar("musMenu", true);
                    }
                    status = telaIni;
                }
                break;
            case inOptions: telaOptions();
                break;
            case pauseGame: telaPausa();
		}

		uniTerminarFrame();
	}
}

void Jogo::startPlay()
{
    nivel = 1;
    pontos = 0;
    contador = 0;
    faseGerada = false;

    esquilo.newGame();
    fase.setSpeedAcres(0);

    if (status == playGame) {
        trafegoSom.tocar(true);
        if (musica) {
            gMusica.tocar("musGame", true);
        }
        fase.inicializar();
    }
}

void Jogo::loadRecursos()
{
    //Personagem
	gRecursos.carregarSpriteSheet("esquilo", "./imagens/esquilo.png", 3, 4);
    gRecursos.carregarSpriteSheet("esquiloMorte", "./imagens/dead_squiq.png", 1, 7);
    //Veiculos
	gRecursos.carregarSpriteSheet("carro", "./imagens/carro.png", 2, 3);
	gRecursos.carregarSpriteSheet("carro2", "./imagens/ferrari-rojo.png", 2, 3);
	gRecursos.carregarSpriteSheet("carro3", "./imagens/customcarbig2.png", 2, 3);
    gRecursos.carregarSpriteSheet("carro4", "./imagens/fusca.png", 2, 3);
    //gRecursos.carregarSpriteSheet("carro3", "./imagens/customcarbig2.png", 2, 3);
    //Cenario
    gRecursos.carregarSpriteSheet("barril", "./imagens/drop.png", 1);
	gRecursos.carregarSpriteSheet("fundoinicial", "./imagens/tela_inici.jpg", 1);
    gRecursos.carregarSpriteSheet("safeCalcada", "./imagens/safe1FULL.png", 1);
    gRecursos.carregarSpriteSheet("halfSafeCalcada", "./imagens/safe1.png", 1);
    gRecursos.carregarSpriteSheet("pista", "./imagens/faixa.png", 1);
    gRecursos.carregarSpriteSheet("halfPista", "./imagens/meia_faixa.png", 1);
    gRecursos.carregarSpriteSheet("forestCenario", "./imagens/back_forest.png", 1);
    //GameOver
    gRecursos.carregarSpriteSheet("gameOver", "./imagens/gameOver2.png", 1);
    //Pause
    gRecursos.carregarSpriteSheet("pause", "./imagens/pause.png", 1);
	gRecursos.carregarSpriteSheet("options", "./imagens/option.png", 1);
	gRecursos.carregarSpriteSheet("botao_resume", "./spritesheets/button_resume.png", 3, 1);
	gRecursos.carregarSpriteSheet("musLiga", "./spritesheets/liga_desliga.png", 2, 1);
    //Noz
    gRecursos.carregarSpriteSheet("the_nut", "./imagens/nut.png", 1);
    gRecursos.carregarSpriteSheet("actions_nut", "./imagens/act_nut.png", 2, 3);
    //Fonte
    gRecursos.carregarFonte("fantasque", "./fontes/FantasqueSansMono-Regular.ttf", 28);
	// Musica da tela inicial
    gRecursos.carregarMusica("musGame", "./sons/DipDaDeeDaDeeDaDohDoh.ogg");
    gRecursos.carregarMusica("musMenu", "./sons/menu.ogg");
    gRecursos.carregarAudio("yay", "./sons/ponto.wav");
    pontoYay.setAudio("yay");
    gRecursos.carregarAudio("boo", "./sons/kids_booing.wav");
    morteBoo.setAudio("boo");
    gRecursos.carregarAudio("trafego", "./sons/trafego2.wav");
    trafegoSom.setAudio("trafego");
    gRecursos.carregarAudio("gameOverSom", "./sons/game_over.wav");
    gameOverSom.setAudio("gameOverSom");
    gRecursos.carregarAudio("somGetNoz", "./sons/squirrel.wav");
    somGetNoz.setAudio("somGetNoz");
    gRecursos.carregarAudio("somGetLife", "./sons/getNoz.wav");
    somGetLife.setAudio("somGetLife");
}

void Jogo::setRecursos() 
{
    fundo.setSpriteSheet(gRecursos.getSpriteSheet("fundo"));
    grama.setSpriteSheet(gRecursos.getSpriteSheet("grama"));
    grama1.setSpriteSheet(gRecursos.getSpriteSheet("grama1"));
    fundoinicial.setSpriteSheet(gRecursos.getSpriteSheet("fundoinicial"));
    gameOver.setSpriteSheet(gRecursos.getSpriteSheet("gameOver"));
    telaPause.setSpriteSheet(gRecursos.getSpriteSheet("pause"));
    dropBarril.setSpriteSheet(gRecursos.getSpriteSheet("barril"));
    optionsTela.setSpriteSheet(gRecursos.getSpriteSheet("options"));

    resume.setSpriteSheet("botao_resume");
    options.setSpriteSheet("botao_option");
    sair.setSpriteSheet("botao_exit");
    somLigar.setSpriteSheet("musLiga");

    resume.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2 - 100);
    options.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
    sair.setPos(gJanela.getLargura() / 2, gJanela.getAltura() / 2 + 100);
    somLigar.setPos(gJanela.getLargura() / 2 + 100, gJanela.getAltura() / 2);

    meuLetreiro.setFonte("fantasque");
    tela.inicializar(meuLetreiro);

    gMusica.setVolumeGlobal(20.0f);
    pontoYay.setVolume(25);
    morteBoo.setVolume(25);
    trafegoSom.setVolume(40);
}
void Jogo::telaInicial() 
{
    if (trafegoSom.estaTocando()){
        trafegoSom.parar();
    }

    pauseEm = "telaIni";
    fundoinicial.desenhar(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
	menu.atualiza(status);
	menu.desenhar();
}

void Jogo::telaCred() 
{
    tela.showCreditos();
    if (gTeclado.pressionou[TECLA_ENTER] || gTeclado.pressionou[TECLA_ENTER2]) {
        status = telaIni;
    }
}

void Jogo::telaOptions() 
{
    if (esquilo.getNoLifes()) {
        esquilo.newGame();
    }
    play();
    optionsTela.desenhar(gJanela.getLargura() / 2, gJanela.getAltura() / 2);

	somLigar.atualizar();
	if (musica) {
        somLigar.setAnimacaoDoEstadoNormal(1);
		somLigar.setAnimacaoDoEstadoComMouseEmCima(0);
    } else {
        somLigar.setAnimacaoDoEstadoNormal(0);
        somLigar.setAnimacaoDoEstadoComMouseEmCima(1);
    }
	
	somLigar.desenhar();

	if (somLigar.estaClicado()){
        musica = !musica;

        if (!musica) {
            gMusica.pausar();
        } else {
            gMusica.continuar();
        }
	}

    if (gTeclado.pressionou[TECLA_ENTER] || gTeclado.pressionou[TECLA_ENTER2]) {
        if (pauseEm == "telaIni") {
            status = telaIni;
        } else {
            status = pauseGame;
        }
    }
}

void Jogo::telaGameOver() {
    play();
    gameOver.desenhar(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
}

void Jogo::telaPausa() {
    play();
    telaPause.desenhar(gJanela.getLargura() / 2, gJanela.getAltura() / 2);
    desenharPause();
    atualizaPause(status);
    if (gTeclado.pressionou[TECLA_ENTER] || gTeclado.pressionou[TECLA_ENTER2]) {
        status = playGame;
    }
}

void Jogo::atualizaPause(Status &status)
{
    //	atualiza todos os botoes
    if (pauseEm == "jogo") {
        resume.atualizar();
    }
    options.atualizar();
    sair.atualizar();

    if (resume.estaClicado()){
        status = playGame;
    }
    if (options.estaClicado()) {
        status = inOptions;
    }
    if (sair.estaClicado()) {
        if (pauseEm == "jogo"){
            if (musica) {
                gMusica.tocar("musMenu", true);
            }
        }
        status = telaIni;
    }
}

void Jogo::desenharPause()
{
    if (pauseEm == "jogo") {
        resume.desenhar();
    }
    options.desenhar();
    sair.desenhar();
}

void Jogo::play()
{
    nivelar();
    fase.desenhaBG();
    dropBarril.desenhar((gJanela.getLargura() / 2) + (gJanela.getLargura() * 0.14), gJanela.getAltura() * 0.95);
    tela.setInterface(nivel, getPontos(), esquilo.getVidas());
    
    if (tela.getAnimacao()) {
        tela.addTimer();
    }

    fase.desenhaPistas();

    if (status == endGame){
        tela.desenha();
        return;
    }

    if (status == pauseGame || status == inOptions) {
        esquilo.desenhar(false);
        fase.desenhaCarros(false);
    } else {
        esquilo.desenhar(true);
        if (!esquilo.getEmAnima()) {
            fase.desenhaCarros(true);
        }
    }
    
    tela.desenha();

    if (esquilo.getNoLifes() && status != inOptions){
        gMusica.parar();
        gameOverSom.tocar(false);
        status = endGame;
        return;
    }

    if (esquilo.getStatus()) {
        testaColisaoVeiculo();
        testaColisaoObjetiva();
    }

    if (gTeclado.pressionou[TECLA_ENTER] || gTeclado.pressionou[TECLA_ENTER2]) {
        status = pauseGame;
    }
}

void Jogo::nivelar()
{
    if (!faseGerada) {
        fase.geraPistas();
        faseGerada = true;
    } else if (contador == 5) {
        fase.geraPistas();
        fase.setSpeedAcres(fase.getSpeedAcress() + 1.5);
        contador = 0;
        nivel++;
        tela.startAnima();
        tela.passaNivel();
        tela.setNivel(nivel);
    }
}

void Jogo::addPontos()
{
	pontos++;
    contador++;
}
int Jogo::getPontos()
{
	return pontos;
}

void Jogo::testaColisaoVeiculo()
{
    bool colisao = fase.testeColisaoVeiculo(esquilo.getPosX(), esquilo.getPosY(), esquilo.getLargura(), esquilo.getAltura() - 35);

    if (colisao) {
        morteBoo.tocar(false);
        esquilo.morre();
        tela.startAnima();
        tela.perdeVida();
        if (esquilo.getNut()) {
            fase.geraNovaNoz();
        }
    }
}

void Jogo::testaColisaoObjetiva()
{
    Tipo colisaoNoz = fase.testeColisaoNoz(esquilo.getPosX(), esquilo.getPosY(), esquilo.getLargura(), esquilo.getAltura() - 35, esquilo.getNut());

    bool colisaoBarril = uniTestarColisaoRetanguloComRetangulo(
        esquilo.getPosX(), esquilo.getPosY(), 0.0, esquilo.getLargura(), esquilo.getAltura(), 0.5, 0,
        (gJanela.getLargura() / 2) + (gJanela.getLargura() * 0.14), gJanela.getAltura() * 0.95, 0.0, dropBarril.getLargura() - 10, dropBarril.getAltura() - 15, 0.5, 0
    );

    if (colisaoNoz != nope && !esquilo.getNut()) {
        if (colisaoNoz == goldNoz) {
            somGetLife.tocar();
            tela.startAnima();
            tela.ganhaVida();
            esquilo.addLife();
        } else{
            somGetNoz.tocar();
        }

        esquilo.pickNut();
    }

    if (colisaoBarril && esquilo.getNut()) {
        pontoYay.tocar(false);
        esquilo.dropNut(1);
        addPontos();
        fase.geraNovaNoz();
        tela.setPontos(getPontos());
    }
}