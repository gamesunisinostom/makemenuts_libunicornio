#include "libUnicornio.h"
#include <time.h>

class Carro
{
public:
    Carro();
    ~Carro();
    void inicializar(string novoSprite, float PosX, float PosY, float Velocidade, char Direcao);
    int getPosX();
    int getPosY();
    float getAltura();
    float getLargura();
    Sprite getSprite();
    void desenhar(bool status);
    bool testaColisao(int testX, int testY, float testLargura, float testAltura);
    bool getColidido();
    void crashCarro();
    void somColisao();
    string randomCar();

protected:
    void goDireita();
    void goEsquerda();
	void setSprite(string nomeSprite);
    void setPosition(int PosX, int PosY);
    void setVelocidade(float Velocidade);
    void changeVelocidade();
    void setAnima(int ID);
    void setDirecao(char Direcao);
    void setSom(string Som);

    Som somCarro;
    Sprite spriteCar;
    float posX, posY, speed;
    char direcao;
    bool colidido;
};