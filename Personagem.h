#include "libUnicornio.h"

class Personagem
{
public:
    Personagem();
    ~Personagem();
	void inicializar(string novoSprite, float PosX, float PosY, float Velocidade);
    void setSprite(string novoSprite, int novoAnima);
    void newGame();
    int getPosX();
    int getPosY();
    int getVidas();
    bool getStatus();
    bool getNoLifes();
    bool getEmAnima();
    void desenhar(bool status);
    void morre();
    void tocaSom();

    bool getNut();
    void pickNut();
    void dropNut(int tipo);
    void addLife();

    float getLargura();
    float getAltura();

    void moveDireita();
    void moveEsquerda();
    void moveCima();
    void moveBaixo();
    void parado();

protected:
    void setPosition(int PosX, int PosY);
    void setVelocidade(int Velocidade);
    void setAnima(int ID);
    void setSom(string Som);
    void setVidas(int vidas);

    Som somPerson;
    Sprite spritePers, sobrePlanoMorte;
    float posX, posY, speed;
    int vidas;
    bool nut, pickingNut, dropingNut, vivo, noLifes, emAnimacao;
};