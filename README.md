# Jogo GrauA Unisinos

Primeiro jogo desenvolvido no curso de Jogos Digitais na Unisinos. Desenvolvido por **Thomas Souza** e **Pietro Paludo**

## Sinopse

> No jogo seu personagem sera um esquilo, que precisa atravessar a rua para buscar nozes a armazená-las em seu barril
  
## Observações

   - Ao fazer o clone do projeto ele deve ter a seguinte estrutura:
   
```
libUnicornio
|---libUnicornio
|---projetos
    |
      ... Demais projetos padrões da libUnicornio
    |
    |---MakeMeNuts
        |--- O CLONE DEVE SER FEITO AQUI
|---README.md
```

## Engine

  - [LibUnicornio](https://github.com/GuilhermeAlanJohann/libUnicornio)

## Licença

  - Esta biblioteca é licenciada sobre o termos da [Licença MIT](http://pt.wikipedia.org/wiki/Licenуa_MIT).

## Agradecimentos

**Fernando Pinho Marson** -- Professor de Algoritmos  
**Guilherme Alan Johann** -- Desenvolvedor da Lib  