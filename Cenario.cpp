#include "Cenario.h"

Cenario::Cenario() {}

Cenario::~Cenario() {}

void Cenario::inicializar(string novoSprite, float novoX, float novoY, Tipo novoTipo, float novaSpeed) 
{
    tipo = novoTipo;
    x = novoX;
    y = novoY;
    speedFaixa = novaSpeed;
    spriteCenario.setSpriteSheet(novoSprite);
    
    if (tipo == noz || tipo == goldNoz) {
        spriteCenario.setEscala(0.9, 0.9);
    } else if (tipo != backGround) {
        spriteCenario.setEscala(1, 0.9);
    }
}

void Cenario::mudaCor(int r, int g, int b, int a)
{
    spriteCenario.setCor(r, g, b, a);
}

void Cenario::desenhar()
{
    spriteCenario.desenhar(x, y);
}

float  Cenario::getX()
{
    return x;
}

float  Cenario::getY()
{
    return y;
}

float Cenario::getAltura()
{
    return spriteCenario.getAltura();
}
float Cenario::getLargura()
{
    return spriteCenario.getLargura();
}

float Cenario::getSpeed()
{
    return speedFaixa;
}

Tipo Cenario::getSpriteType()
{
    return tipo;
}

bool Cenario::getNozPega()
{
    return nozPega;
}
void Cenario::setNozPega(bool pega)
{
    nozPega = pega;
}