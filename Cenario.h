#include "libUnicornio.h"

enum Tipo {
    pista, safeZone, noz, goldNoz, nope, backGround
};

class Cenario
{
public:
    Cenario();
    ~Cenario();
    void inicializar(string spriteNova, float PosX, float PosY, Tipo novoTipo, float NovaSpeed);
    void desenhar();
    void mudaCor(int r, int g, int b, int a);
    float getX();
    float getY();
    float getAltura();
    float getLargura();
    float getSpeed();
    bool getNozPega();
    void setNozPega(bool pega);
    Tipo getSpriteType();
    
protected:
    float x, y, altura, largura ,speedFaixa;;
    Tipo tipo;
    Sprite spriteCenario;
    bool nozPega = false;
};