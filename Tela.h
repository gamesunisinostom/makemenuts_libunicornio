#include "libUnicornio.h"
#include <iomanip>
#include <locale>
#include <sstream>

class Tela 
{
public:
    Tela();
    ~Tela();
    void setInterface(int nNivel, int nPonto, int nVida);
    void desenha();
    void inicializar(Texto fonte);
    void setNivel(int nivel);
    void setPontos(int pontos);
    void setVidas(int vidas);
    void showCreditos();
    void perdeVida();
    void ganhaVida();
    void passaNivel();
    void resetMostruarios();
    void startAnima();
    void addTimer();
    bool getAnimacao();

protected:
    Texto pontos, vidas, nivel, creditos;
    Sprite telaPonto, telaVida, telaNivel;
    int tempoAnima;
    bool animacao;
};