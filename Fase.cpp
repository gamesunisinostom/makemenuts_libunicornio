#include "Fase.h"

Fase::Fase() {}

Fase::~Fase() {}

void Fase::inicializar()
{
    bkground.inicializar("forestCenario", gJanela.getLargura() / 2, gJanela.getAltura() / 2, backGround, 0);
    geraPistas();
}

void Fase::desenhaBG()
{
    bkground.desenhar();
}

void Fase::geraPistas()
{
    srand(time(0));

    for (int i = 0; i < 7; i++) {

        y[i] = 100 * (i +1) + 45;

        int tipo = rand() % 3;

        if (tipo != 0 || lastTipo == 1) {
            spritePistas[i].inicializar("pista", gJanela.getLargura() / 2, y[i], pista, rand() % (50 + 20) / 10.0);
            lastTipo = 0;
        }
        else {;
        spritePistas[i].inicializar("halfSafeCalcada", gJanela.getLargura() / 2, y[i], safeZone, 0);
            lastTipo = 1;
        }
    }

    for (int j = 0; j < 5; j++) {
        spriteNuts[j].inicializar("the_nut", rand() % 220 + gJanela.getLargura() / 2, rand()% 30 + 50, noz, 0);
        if (spriteNuts[j].getNozPega()) {
            spriteNuts[j].setNozPega(false);
        }
    }

    insertCars();
}

void Fase::insertCars()
{
    string carNome;
    for (int i = 0; i < 7; i++) {
        if (spritePistas[i].getSpriteType() == 0) {
            carNome = randomCar();
            if (rand() % 2 == 0) {
                veiculos[i].inicializar(carNome, gJanela.getLargura() /2  - gJanela.getLargura() * 0.3,
                    spritePistas[i].getY() + 12, spritePistas[i].getSpeed() + speedAcres, 'd'
                );
            } else {
				veiculos[i].inicializar(carNome, gJanela.getLargura() / 2 + gJanela.getLargura() * 0.3,
                    spritePistas[i].getY() - 28, spritePistas[i].getSpeed() + speedAcres, 'e'
                );
            }
        }
    }
}

void Fase::desenhaPistas()
{
    for (int i = 0; i < 7; i++) {
        spritePistas[i].desenhar();
    }
    for (int j = 0; j < 5; j++) {
        if (!spriteNuts[j].getNozPega()) {
            if (spriteNuts[j].getSpriteType() == goldNoz) {
                spriteNuts[j].mudaCor(255, 215, 0, 255);
            } else {
                spriteNuts[j].mudaCor(255, 255, 255, 255);
            }

            spriteNuts[j].desenhar();
        } else {
            if (rand() % 500 == 15) {
                if (rand() % 10 == 8) {
                    spriteNuts[j].inicializar("the_nut", rand() % 220 + 360, rand() % 30 + 50, goldNoz, 0);
                } else {
                    spriteNuts[j].inicializar("the_nut", rand() % 220 + 360, rand() % 30 + 50, noz, 0);
                }
                spriteNuts[j].setNozPega(false);
            }
        }
    }
}

void Fase::desenhaCarros(bool status)
{
    for (int i = 0; i < 7; i++) {
        if (spritePistas[i].getSpriteType() == 0) {
            veiculos[i].desenhar(status);
        }
    }
}


bool Fase::testeColisaoVeiculo(int testX, int testY, float testLargura, float testAltura)
{
    bool colisao = false;
    for (int i = 0; i < 7; i++) {
        if (colisao)
            break;

        if (spritePistas[i].getSpriteType() == 0) {
            colisao = veiculos[i].testaColisao(testX, testY, testLargura, testAltura);
        }
    }

    return colisao;
}

Tipo Fase::testeColisaoNoz(int testX, int testY, float testLargura, float testAltura, bool temNoz)
{
    bool colisao = false;
    Tipo retorno;

    if (temNoz) {
        retorno = nope;
        return retorno;
    }

    for (int j = 0; j < 5; j++) {
        if (colisao) {            
            break;
        }
        if (!spriteNuts[j].getNozPega()) {
            if (colisao = uniTestarColisaoRetanguloComRetangulo(
                testX, testY, 0.0, testLargura, testAltura, 0.5, 0, spriteNuts[j].getX(), spriteNuts[j].getY(), 0.0, spriteNuts[j].getLargura(), spriteNuts[j].getAltura(), 0.5, 0.5 )) {
                colisao = true;
                spriteNuts[j].setNozPega(true);
                if (spriteNuts[j].getSpriteType() == noz) {
                    retorno =  noz;
                } else if (spriteNuts[j].getSpriteType() == goldNoz) {
                    retorno  = goldNoz;
                }
            }
        }
    }
    if (colisao) {
        return retorno;
    } else {
        return nope;
    }
    
}

void Fase::geraNovaNoz()
{
    if (spriteNuts[1].getNozPega()) {
        spriteNuts[1].setNozPega(false);
    }
}

string Fase::randomCar()
{
    int tipo = rand() % 4;
    switch (tipo){
        case 0:
            return "carro";
        case 1:
            return "carro2";
        case 2:
            return "carro3";
        case 3:
            return "carro4";
    }
}

float Fase::getSpeedAcress()
{
	return speedAcres;
}

void Fase::setSpeedAcres(float nSpeed) {
	speedAcres = nSpeed;
}