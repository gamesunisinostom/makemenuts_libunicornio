#include "libUnicornio.h"
#include "Carro.h"
#include "Cenario.h"
#include <time.h>

class Fase 
{
public:
    Fase();
    ~Fase();
    void inicializar();
    void insertCars();
    void geraPistas();
    void desenhaBG();
    void desenhaPistas();
    void desenhaCarros(bool status);
    void geraSafeZone();
    void geraNovaNoz();
    bool testeColisaoVeiculo(int testX, int testY, float testLargura, float testAltura);
    Tipo testeColisaoNoz(int testX, int testY, float testLargura, float testAltura, bool temNoz);
	void setSpeedAcres(float nSpeed);
	float getSpeedAcress();
    string randomCar();

protected:
    Carro veiculos[10];
    Cenario spritePistas[6];
    Cenario spriteSafeZones[6];
    Cenario spriteNuts[5];
    Cenario bkground;
    int x = 450, y[6], lastTipo = 10;
	float  speedAcres = 0;
    bool nozPega = false;
};