#include "Personagem.h"

Personagem::Personagem() 
{ 
    vidas = 3;
    vivo = true;
    noLifes = false;
    nut = false;
    pickingNut = false;
    dropingNut = false;
    emAnimacao = false;
}

Personagem::~Personagem() { }

void Personagem::inicializar(string spriteNova, float PosX, float PosY, float velocidade)
{
	spritePers.setSpriteSheet(spriteNova);
	speed = velocidade;
	setPosition(PosX, PosY);
    spritePers.setEscala(0.8, 0.8);

    gRecursos.carregarSpriteSheet("sobMorte", "./imagens/sobreplanoMorte.png", 1);
    sobrePlanoMorte.setSpriteSheet("sobMorte");
}

void Personagem::newGame()
{
    vidas = 3;
    vivo = true;
    noLifes = false;
    setPosition((gJanela.getLargura() / 2) , gJanela.getAltura() * 0.95);
}

void Personagem::setSprite(string novoSprite, int novoAnima)
{
    spritePers.setSpriteSheet(novoSprite);
    spritePers.setAnimacao(novoAnima);
}

float Personagem::getAltura()
{
	return spritePers.getAltura();
}
float Personagem::getLargura()
{
	return spritePers.getLargura();
}

void Personagem::setPosition(int novoPosX, int novoPosY)
{
    posX = novoPosX;
    posY = novoPosY;
}
int Personagem::getPosX()
{
    return posX;
}
int Personagem::getPosY()
{
    return posY;
}

void Personagem::setVelocidade(int Velocidade)
{
    speed = Velocidade;
}

void Personagem::addLife()
{
    vidas++;
}

void Personagem::moveDireita()
{
	spritePers.setAnimacao(2);
	spritePers.setInverterX(false);
    int largura = gJanela.getLargura();

    if (posX < largura) {
            posX += speed;
    }
}
void Personagem::moveEsquerda()
{
	spritePers.setAnimacao(2);
	spritePers.setInverterX(true);
    if (posX > 0) {
        posX -= speed;
    }
}
void Personagem::moveCima()
{
    spritePers.setAnimacao(0);
    if (posY > 0) {
        posY -= speed;
    }
}
void Personagem::moveBaixo()
{
	spritePers.setAnimacao(1);
    int altura = gJanela.getAltura();

    if (posY < altura) {
        posY += speed;
    }
}
void Personagem::parado()
{
    if (!pickingNut) {
        if (posY < 100 || nut) {
            spritePers.setAnimacao(1);
        }
        else {
            spritePers.setAnimacao(0);
        }
        spritePers.setFrame(0);
    }
}

void Personagem::setAnima(int ID)
{
	spritePers.setAnimacao(ID);
}

bool Personagem::getNut()
{
    return nut;
}
void Personagem::pickNut()
{
    nut = true;
    pickingNut = true;
    setSprite("actions_nut", 0);
}

void Personagem::dropNut(int tipo)
{
    nut = false;
    dropingNut = true;
    if (tipo == 1) {
        setSprite("actions_nut", 1);
    }
}

void Personagem::desenhar(bool status)
{
    if (vivo) {

        if (pickingNut) {
            spritePers.desenhar(posX, posY);
            spritePers.setVelocidadeAnimacao(0.8);
            spritePers.avancarAnimacao();

            if (spritePers.getFrame() == 2) {
                pickingNut = false;
                setSprite("esquilo", 0);
            }
            return;
        }
        if (dropingNut) {
            if (spritePers.getFrame() == 1) {
                dropingNut = false;
                setSprite("esquilo", 0);
            }

            spritePers.setVelocidadeAnimacao(0.5);
            spritePers.avancarAnimacao();
            spritePers.desenhar(posX, posY);
            
            return;
        }

        if (status) {
            if (gTeclado.segurando[TECLA_DIR] || gTeclado.segurando[TECLA_D]) {
                moveDireita();
                spritePers.avancarAnimacao();
            }
            else if (gTeclado.segurando[TECLA_ESQ] || gTeclado.segurando[TECLA_A]) {
                moveEsquerda();
                spritePers.avancarAnimacao();
            }
            else if (gTeclado.segurando[TECLA_CIMA] || gTeclado.segurando[TECLA_W]) {
                moveCima();
                spritePers.avancarAnimacao();
            }
            else if (gTeclado.segurando[TECLA_BAIXO] || gTeclado.segurando[TECLA_S]) {
                moveBaixo();
                spritePers.avancarAnimacao();
            }
            else {
                parado();
            }
        }

		spritePers.setVelocidadeAnimacao(7);
		spritePers.desenhar(posX, posY);
    } else if (!vivo){
        emAnimacao = true;
        if (spritePers.getFrame() == 6) {
            emAnimacao = false;
            spritePers.setSpriteSheet("esquilo");
            setPosition(400, 850);
            if (vidas < 1) {
                noLifes = true;
                return;
            } else {
                vivo = true;
            }
        }
        
        spritePers.desenhar(posX, posY);
        sobrePlanoMorte.desenhar(posX, posY);
        
        spritePers.setVelocidadeAnimacao(2);
        spritePers.avancarAnimacao();
    }
}

bool Personagem::getStatus()
{
    return vivo;
}

bool Personagem::getNoLifes()
{
    return noLifes;
}

void Personagem::morre()
{
    setSprite("esquiloMorte", 0);
    dropNut(0);
    vivo = false;
    setVidas(getVidas() - 1);
}

void Personagem::setVidas(int vid)
{
    vidas = vid;
}
int Personagem::getVidas()
{
    return vidas;
}

void Personagem::setSom(string novoSom)
{
    somPerson.setAudio(novoSom);
}

void Personagem::tocaSom()
{
    somPerson.tocar();
}

bool Personagem::getEmAnima()
{
    return emAnimacao;
}