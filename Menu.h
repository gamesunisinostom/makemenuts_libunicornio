#include "libUnicornio.h"

enum Status{
	telaIni, startGame, playGame, endGame, inOptions, outGame, telaCreditos, pauseGame
};

class Menu
{
public:
    Menu();
    ~Menu();

    void inicializar();
    void finalizar();

    void atualiza(Status &status);
    void desenhar();

private:
    BotaoSprite jogar;
	BotaoSprite option;
	BotaoSprite credits;
    BotaoSprite sair;
	Musica musica1;
};